<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style2.css">
</head>
<body>
	<div>
		<img src="Images/pic3.jpg" alt="Avatar" class="avatar">
		<form action="" method="GET">
			<h2><br>Sign Up</h2>
			
			<input type="text" placeholder="Name" name="name" id="name">
			<span id="nameError" style="color:red;display:none;margin-left:10px;">User name must not be empty</span>
			<br><br>
			
			
			<input type="text" placeholder="Phone no." name="number" id="phone">
			<span id="mobileError" style="color:red;display:none;margin-left:10px;">mobile no.must not be empty</span><br><br>

			
			<input type="text" placeholder="abc@gmail.com" name="email" id="email">
			<span id="userError" style="color:red;display:none;margin-left:10px;">Email must not be empty</span><br><br>

			<b><p style="margin-left: 45px;color: black;">
				Gender:
				<input type="radio" name="gender" value="m">Male
				<input type="radio" name="gender" value="f">Female
				<input type="radio" name="gender" value="o">Others</p></b><br>


				<input type="Password" placeholder="Password" name="password" id="pass1"><br>
				<span id="pass1Error" style="color:red;display:none;margin-left:10px;"> password must not be empty</span><br>


				<input type="Password" placeholder="Confirm password" name="confirmpass" id="pass2">
				<span id="pass2Error" style="color:red;display:none;margin-left:10px;"></span><br><br>

				<input type="text" placeholder="Write here....." style="height: 
				80px" name="bio"><br><br><br>

				<input type="submit" value="SIGN UP" name="sign up" id="login">
				<p style="margin-left: 44px;margin-top: 17px;">Already a user<a href="login.php" style="color: aqua;">Sign in</a></p>
				

			</form><br>

		</div>
		<script type="text/javascript" src="js/jquery.js"/></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$('#login').click(function(){ 
					var name=$('#name');
					var phone=$('#phone');
					var email=$('#email');
					var pass1=$('#pass1');
					var pass2=$('#pass2');
					if(name.val()==''){
						$('#nameError').show();
						name.focus();
						return false;
					} else {
						$('#nameError').hide();
					}

					if ( name.val().match('^[a-zA-Z]{3,16}$') ) {

					} else {
						$('#nameError').text('invalid name');
						$('#nameError').show();
						name.focus();
						return false;
					}


					if(phone.val()==''){
						$('#mobileError').show();
						phone.focus();
						return false;

					} else {
						$('#mobileError').hide();

					}
					if ( phone.val().match('^[0-9]{10}$') ) {
					} else {
						$('#mobileError').text('invalid phone');
						$('#mobileError').show();
						phone.focus();
						return false;
					}
					if(email.val()==''){
						$('#userError').show();
						email.focus();
						return false;

					}  else {
						$('#userError').hide();

					}
					if(!isEmail(email.val())) {
						$('#userError').text('invalid email format');
						$('#userError').show();
						email.focus();
						return false;
					}
					if(pass1.val()==''){
						$('#pass1Error').show();
						pass1.focus();
						return false;

					} 

					if(pass1.val().length<8)
					{
						$('#pass1Error').text('password must greater than 8 characters');
						$('#pass1Error').show();
						return false;
					} else {
						$('#pass1Error').hide();
					}


					console.log(pass1.val());
					console.log(pass2.val());
					if(pass1.val() != pass2.val()) {
						$('#pass2Error').text('password 1 and 2 should be same');
						$('#pass2Error').show();
						return false;
					}
					else{
						$('#pass2Error').hide();
					}
					
				});

			});
			function isEmail(email) {
				var r = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				if(r.test(email)) {
					return true;
				}
				else{
					return false;

				}
			}


			function isPass1(pass1){ 
				var p= /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])$/;
				if (p.test(pass1)) {
					return true;
				}
				else{
					return false;
				}
			}


		</script>
		<?php 
		$conn=mysqli_connect('localhost','root','','test');


		if (isset($_GET['name']) && isset($_GET['email']) && isset($_GET['number']) && isset($_GET['gender']) && isset($_GET['password']) && isset($_GET['bio'])){
		$name=$_GET['name'];
		$email=$_GET['email'];
		$number=$_GET['number'];
		$gender=$_GET['gender'];
		$password=$_GET['password'];
		$bio=$_GET['bio'];

		$sql="insert into users(name,email,number,gender,password,bio) values('$name','$email','$number','$gender','$password','$bio')";
		$result=mysqli_query($conn,$sql);
		if ($result) {
		echo 'user inserted successfully';

	}else{
	echo 'some error occurred';
}


}
?>
</body>



<?php
$result=mysqli_query($conn,'select * from users');
?>
<table border="1">
	<?php

	while ($row=mysqli_fetch_assoc($result)) {

	?>
	<tr>
		<td><?php echo $row['id'];?></td>
		<td><?php echo $row['name'];?></td>
		<td><?php echo $row['email'];?></td>
		<td><?php echo $row['number'];?></td>
		<td><?php echo $row['gender'];?></td>
		<td><?php echo $row['password'];?></td>
		<td><?php echo $row['bio'];?></td>
	</tr>
	<?php
}
?>
</table>

</html>